﻿﻿List<int> numbers = new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
Console.WriteLine("Initial list: ");
foreach (var number in numbers)
{
    Console.Write(number + " ");
}
List<int> numbersGreaterThan4 = numbers.Where(x => x > 4);
Console.WriteLine("\nResult list number greater than 4:");
foreach (var number in numbersGreaterThan4)
{
    Console.Write(number + " ");
}
List<string> strings = new List<string> { "Hello", "Hi", "abc", "vtsdsdr", "mnsadsl" };
Console.WriteLine("\nInitial list: ");
foreach (var item in strings)
{
    Console.Write(item + " ");
}
List<string> stringsHaveLengthGreaterThan7 = strings.Where(x => x.Length > 7);
Console.WriteLine("\nResult list string have lengh more than 7: ");
foreach (var item in stringsHaveLengthGreaterThan7)
{
    Console.Write(item + " ");
}