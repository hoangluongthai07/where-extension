﻿public static class ListExtension
{
    public static List<T> Where<T>(this List<T> numbers, Predicate<T> predicate)
    {
        List<T> result = new List<T>();
        foreach (T item in numbers)
        {
            if (predicate(item))
            {
                result.Add(item);
            }
        }
        return result;
    }
}